<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <title>Aplicación Basketball</title>
    <link rel="icon" type="image/x-icon" href="img/favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Orbitron' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='css/style.css' >
	  <link rel="stylesheet"  href="css/estilo.css"> 
    <link rel="stylesheet"  href="css/chosen.css">
    <link rel="stylesheet" href="css/animate.css">
    <script src="js/jquery-1.11.3.min.js"></script> 
    <script type="text/javascript" src="js/interface.js"></script>
	  <script type="text/javascript" src="js/accion.js"></script>
  
  </head>
<body>

<div id="contenedor"> 
    <?php
    include("marcador.php");
    ?>
</div>

<div class="dock" id="dock">
  <div class="dock-container">
    <?php
    include("menu.php");
    ?>
  </div>
</div>

<script src="js/prism.js"></script>
<script src="js/chosen.jquery.js"></script>

</body>
</html>
