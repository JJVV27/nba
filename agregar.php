
<h2>Agregar Equipo</h2>


<div class="agregarEquipo animated fadeIn">

    <h3>Datos del Equipo</h3>

        <div id="imgEquipo">
        	<div class="btn btn-success fileinput-button">
             	<img src="img/team.png" alt="Seleccionar Imagen" title="Seleccionar Imagen de Equipo" id="team">
        		<form id="formulario" action=""enctype="multipart/form-data">
        		<input class="imgTeam" type="file" name="image_upload" id="image_upload">
        		</form>
            </div>
        </div>

        <div id="datosEquipo">
                <label>Nombre:</label><input type="text" id="nombre_equipo" name="nombre" value="">
                <br>
                <label>Abreviatura:</label><input type="text" id="abreviatura_equipo" name="abreviatura" value="">
                <br>
                <label>Seudónimo:</label><input type="text" id="apodo_equipo" name="apodo" value="">
                <br>
                <label>Categoría:</label><input type="text" id="categoria_equipo" name="categoria" value="">
        
        </div>

</div>

<div id="play">
 	<a class="button" id="agregar_equipo" >
        <span>Guardar Equipo</span>
    </a>
</div>
