$(function () {
    function restrict_multiple(selector) {
        // Aqui establece el valor actual en su alt
        $(selector).each(function () {
            $(this).attr("alt", $(this).val());
        })
        // Disparador cuando cambia el select
        $(selector).change(function () {
            // Eliminando el hidden del option
            $(selector + " option").removeClass("hidden");
            
            // Se usa el alt attr, como aun auxiliar para mantener el valor que esta activo
            $(this).attr("alt", $(this).val())
            
            // Creando un arreglo con las opciones seleccionadas
            var selected = new Array();
            
            // Cada opcion seleccionada se ingresa en el arreglo
            $(selector + " option:selected").each(function () {
                selected.push(this.value);
            })
            
            // Ocultando los seleccionados ya, para no verlos en los demas selects
            for (k in selected) {
                $(selector + "[alt!=" + selected[k] + "] option[value=" + selected[k] + "]").addClass("hidden")
            }
        })
        
        // Disparador para que se mantenga actualizado todos los selects
        $(selector).each(function () { $(this).trigger("change"); })
    }
    
    restrict_multiple(".chosen-select");
})