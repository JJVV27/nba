
$(document).ready(function(){

$('#contenedor').on("click","#partido",function(){
	var nombreLocal = $("#nombreLocal ").val();
	var nombreVisitante = $("#nombreVisitante ").val();
		

		if (nombreLocal == ""){
			alert("Por favor Seleccionar Equipo Local");
			return false;
		}else if(nombreVisitante == ""){
			alert("Por favor Seleccionar Equipo Visitante");
			return false;
		}else if (nombreLocal == nombreVisitante){
			alert("Este equipo ya ha sido seleccionado. \nPor favor elegir otro equipo.");
			return false;
		}else if(nombreVisitante == nombreLocal){
			alert("Este equipo ya ha sido seleccionado. \nPor favor elegir otro equipo.");
			return false;
		}

	$.ajax({

            data: "nombreLocal=" + nombreLocal + "&" +  "nombreVisitante=" + nombreVisitante + "&" + "tipo_accion=partido" ,
            url: "include/_juego.php",
            type: "post",
            
      		success: function(Datosrecibidos){
			$('#contenedor').empty();
			$('#contenedor').append(Datosrecibidos); 
			$('#contenedor').load("marcador.php");
            },
        });

});

$('#contenedor').on("click","#agregar_equipo",function(){
	var nombreEquipo = $("#nombre_equipo").val();
	var abreviaturaEquipo = $("#abreviatura_equipo").val();
	var apodoEquipo = $("#apodo_equipo").val();
	var categoriaEquipo = $("#categoria_equipo").val();
	var imagenEquipo = $("#image_upload").val();

	if (imagenEquipo == ""){
	alert("Por favor Agregar Imagen de Equipo");
	return false;
	}else if (nombreEquipo == ""){
	alert("Por favor Agregar Nombre de Equipo");
	return false;
	}else if(abreviaturaEquipo == ""){
	alert("Por favor Agregar Abreviatura de Equipo");
	return false;
	}else if(apodoEquipo == ""){
	alert("Por favor Agregar el Seudónimo de Equipo");
	return false;
	}else if(categoriaEquipo == ""){
	alert("Por favor Agregar el Categoria de Equipo");
	return false;
	}

	$.ajax({

            data: "nombreEquipo=" + nombreEquipo + "&" + "abreviaturaEquipo=" + abreviaturaEquipo + "&" + "apodoEquipo=" + apodoEquipo + "&" + "categoriaEquipo=" + categoriaEquipo + "&"  + "tipo_accion=agregar",
            url: "include/_agregar.php",
            type: "post",
            
				success: function(data){
					subir_img();
					alert ("Los datos han sido almacenados exitosamente");
					$('#contenedor').empty();
					$('#contenedor').append(data); 
					$('#contenedor').load("juego.php");
            },
        });


});


$("#contenedor").on("change","#image_upload",function(data){
var tmppath = URL.createObjectURL(data.target.files[0]);
var input = document.getElementById('image_upload').value.lastIndexOf(".png");
if(input==-1)
     {
      alert('Solo admite imagenes .png');
      return false;
     }else{
$("#team").attr('src',tmppath);
}
});



//FUNCIÓN SUBIR IMAGEN 
function subir_img(){
var formData = new FormData($("#formulario")[0]);

$.ajax({
url:'_upload.php',
data:formData,
type:'post',
contentType:false,
processData:false
});
}

});


$(document).ready(function(){
//BOTONES MENU DOCK 
//
$('#dock').on("click","#btn1",function(){
		$("#contenedor").empty();
		$("#contenedor").load("agregar.php");
		});

$('#dock').on("click","#btn2",function(){
		$("#contenedor").empty();
		$("#contenedor").load("juego.php");
		});
$('#dock').on("click","#btn3",function(){
		$("#contenedor").empty();
		$("#contenedor").load("marcador.php");
		});

$('#dock').Fisheye(
				{
					maxWidth: 20,
					items: 'a',
					itemsText: 'span',
					container: '.dock-container',
					itemWidth: 50,
					proximity: 10,
					halign : 'center'
				}
			);

});


//FUNCIÓN CARGAR IMAGEN SELECT
function cargar_imagenLocal(imagen) {
    var img = document.getElementById("localLogo");
    var img_dir = "equipos/" ;
    if (img) {
    img.src = img_dir + imagen + ".png";
    }
    }
function cargar_imagenVisitante(imagen) {
    var img = document.getElementById("visitanteLogo");
    var img_dir = "equipos/";
    if (img) {
    img.src = img_dir + imagen + ".png";
    }
    }
    